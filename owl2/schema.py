import ipaddress
import re
import socket

import voluptuous as vo

from .utils import AttrDict


class AttrSchema(vo.Schema):
    """Attribute schema.

    Allows to access schema items by attribute.
    """

    def __call__(self, d):
        return AttrDict(super().__call__(d))


def IpAddress():
    def checker(address):
        try:
            ipaddress.ip_address(address)
        except ValueError:
            raise vo.Invalid(f"Invalid IP address: {address}")
        return address

    return checker


def Hostname():
    def checker(name):
        try:
            address = socket.gethostbyname(name)
        except ValueError:
            raise vo.Invalid(f"Invalid IP address: {name}")
        return address

    return checker


def Choice(kind, choices):
    def checker(value):
        if value not in choices:
            raise vo.Invalid(f"{value} not valid for {kind}")
        return value

    return checker


def DockerImage():
    def checker(value):
        m = re.compile(r"([a-z]+)/([a-z0-9-):([a-z0-9.]+)?").match(value)
        if m is None:
            raise vo.Invalid(f"Invalid Docker image: {value!r}")
        return value

    return checker


def MemorySpec():
    def checker(value):
        if isinstance(value, int):
            return f"{value}Gi"

        try:
            value = str(value)
        except Exception:
            raise vo.Invalid(f"Invalid memory: {value!r}")
        return value

    return checker


def CpuSpec():
    def checker(value):
        try:
            value = str(value)
        except Exception:
            raise vo.Invalid(f"Invalid cpu: {value!r}")
        return value

    return checker


def _validate_scheduler(conf):
    return conf


schema_resources = AttrSchema(
    {
        vo.Optional("workers", default=3): vo.All(int, vo.Range(min=1, max=100)),
        vo.Optional("memory", default=7): MemorySpec(),
        vo.Optional("cpu", default=2): CpuSpec(),
        vo.Optional("retries", default=0): vo.All(int, vo.Range(min=0, max=10)),
        vo.Optional("image"): DockerImage(),
        vo.Optional("volumes", default=[]): list,
    }
)

schema_python = AttrSchema(
    {
        vo.Optional("virtualenv", default=""): str,
        vo.Optional("reset_virtualenv", default=""): str,
    }
)

schema_pipeline = AttrSchema(
    {
        vo.Required("resources"): schema_resources,
        vo.Optional("python", default=schema_python({})): schema_python,
    },
    extra=vo.ALLOW_EXTRA,
)
