import datetime
import socket
from typing import Any, Dict, Optional

import databases
import jwt
from fastapi import Depends, FastAPI, Header, HTTPException, Request, status
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer
from pydantic import BaseModel

from .. import database as db
from ..config import config
from ..crypto import PBKDF2PasswordHasher

database = databases.Database(config["dbi"])
hostname = socket.gethostname()
local_ip = socket.gethostbyname(hostname)
app = FastAPI()


# Define the HTTP Bearer security scheme
bearer_scheme = HTTPBearer()


# Define a request model for the POST route
class Message(BaseModel):
    message: str


class User(BaseModel):
    username: str
    password: Optional[str] = None


class Pipeline(BaseModel):
    config: Dict[str, Any]


@app.on_event("startup")
async def startup():
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()


# Define the authentication dependency
def authenticate(credentials: HTTPAuthorizationCredentials = Depends(bearer_scheme)):
    # Check if the token matches the secret token
    if credentials.scheme != "Bearer":
        raise HTTPException(status_code=401, detail="Invalid authentication scheme")

    try:
        decoded = jwt.decode(
            credentials.credentials, config["secret_token"], algorithms=["HS256"]
        )
    except jwt.exceptions.DecodeError as e:
        raise HTTPException(status_code=401, detail="Invalid token") from e
    except jwt.exceptions.ExpiredSignatureError as e:
        raise HTTPException(status_code=401, detail="Token expired") from e

    return decoded


async def check_password(user) -> bool:
    q = db.UserAdmin.select().where(
        db.User.c.username == user.username, db.User.c.is_active == True
    )
    res = await database.fetch_one(q)
    if not res:
        return False
    hasher = PBKDF2PasswordHasher()
    return hasher.verify(user.password, res["password"])


@app.post("/api/auth/login")
async def login(user: User):
    res = await check_password(user)
    if not res:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
        )

    key = config["secret_token"]
    exp = datetime.datetime.now(tz=datetime.timezone.utc) + datetime.timedelta(hours=6)
    encoded = jwt.encode(
        {"username": user.username, "exp": exp}, key, algorithm="HS256"
    )
    return {"token": encoded}


@app.get("/api/auth/me")
async def me(auth: dict = Depends(authenticate)):
    return {"username": auth["username"]}


@app.get("/api/pipeline/status/{uid}")
async def pipeline_status_get(uid: int, auth: dict = Depends(authenticate)):
    q = db.Pipeline.select().where(db.Pipeline.c.id == uid)
    res = await database.fetch_one(q)
    return res


@app.post("/api/pipeline/submit")
async def pipeline_submit(pipe: Pipeline, auth: dict = Depends(authenticate)):
    if not pipe.config:
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail="Empty config",
        )

    name = pipe.config["name"]
    q = db.PipelineDefinition.select().where(
        db.PipelineDefinition.pipeline_name == name
    )
    pdef = await database.fetch_one(q)

    q = db.User.select().where(db.User.c.username == auth["username"])
    user = await database.fetch_one(q)

    q = db.Pipeline.insert().values(
        owner_id=user.id,
        pdef_id=pdef.id,
        config=pipe.config,
        created_at=datetime.now(),
        status="PENDING",
    )
    uid = await database.execute(q)
    return {"id": uid}
