import asyncio
import logging
import os
import shutil
import traceback
from contextlib import suppress
from functools import wraps
from pathlib import Path
from typing import Callable, Dict

import pkg_resources
import voluptuous as vo
import yaml
from distributed import Client

from ..log import logconf
from ..plugins import (
    PipInstall,
    ProcessPoolPlugin,
    SchedulerLoggingPlugin,
    WorkerLoggingPlugin,
)

logger = logging.getLogger("owl2.pipeline")

PIPELINES = {}


def _import_pipelines():
    for e in pkg_resources.iter_entry_points("owl.pipelines"):
        f = e.load()
        try:
            schema = f.schema
        except AttributeError:
            schema = None
        PIPELINES[e.name] = register_pipeline(validate=schema)(f)


class register_pipeline:
    """Register a pipeline."""

    def __init__(self, *, validate: vo.Schema = None):
        self.schema = validate
        if self.schema is not None:
            self.schema.extra = vo.REMOVE_EXTRA

    def __call__(self, func: Callable):
        name = func.__name__

        @wraps(func)
        def wrapper(config: Dict, loop: asyncio.AbstractEventLoop = None):
            _config = self.schema(config) if self.schema is not None else config

            if loop is not None:
                asyncio.set_event_loop(loop)
            else:
                try:
                    loop = asyncio.get_event_loop()
                except RuntimeError:
                    loop = asyncio.new_event_loop()
                    asyncio.set_event_loop(loop)

            client = Client()
            client.register_worker_plugin(PipInstall([config["pipeline_pkg"]]))
            client.register_worker_plugin(WorkerLoggingPlugin(logconf))
            client.register_scheduler_plugin(SchedulerLoggingPlugin(logconf))
            client.register_worker_plugin(ProcessPoolPlugin())

            try:
                func.main.config = _config
                return func.main(**_config)
            except Exception:
                traceback_str = traceback.format_exc()
                raise Exception(
                    "Error occurred. Original traceback " "is\n%s\n" % traceback_str
                )
            finally:
                client.close()
                logfile = os.environ.get("LOGFILE")
                with suppress(Exception):
                    path = Path(logfile)
                    shutil.copy(path, Path(func.main.config["output_dir"]) / path.name)

        PIPELINES[name] = wrapper
        return wrapper


def __getattr__(name: str):
    """Return a named plugin"""
    try:
        return PIPELINES[name]
    except KeyError:
        _import_pipelines()
        if name in PIPELINES:
            return PIPELINES[name]
        else:
            raise AttributeError(
                f"module {__name__!r} has no attribute {name!r}"
            ) from None


def __dir__():
    """List available plug-ins"""
    _import_pipelines()
    return list(PIPELINES.keys())
