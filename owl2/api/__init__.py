import socket
from pathlib import Path
from typing import Any, Dict, Optional

import databases
from elasticsearch7 import AsyncElasticsearch, Elasticsearch
from fastapi import Depends, FastAPI, Header, HTTPException, Request
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer
from pydantic import BaseModel

from .. import database as db
from ..config import config

database = databases.Database(config["dbi"])
hostname = socket.gethostname()
local_ip = socket.gethostbyname(hostname)
app = FastAPI()
es = AsyncElasticsearch(config["es_host"])

# Define the HTTP Bearer security scheme
bearer_scheme = HTTPBearer()


# Define a request model for the POST route
class Message(BaseModel):
    message: str


class Status(BaseModel):
    status: str
    heartbeat: Optional[Dict[str, Any]] = None


@app.on_event("startup")
async def startup():
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()
    await es.close()


# Define the authentication dependency
def authenticate(credentials: HTTPAuthorizationCredentials = Depends(bearer_scheme)):
    # Check if the token matches the secret token
    if credentials.scheme != "Bearer":
        raise HTTPException(status_code=401, detail="Invalid authentication scheme")
    elif credentials.credentials != config["secret_token"]:
        print(credentials.credentials, config["secret_token"])
        raise HTTPException(status_code=401, detail="Invalid token")


@app.get("/api/auth/token/handshake")
async def handshake(auth: bool = Depends(authenticate)):
    return {"detail": "ok"}


@app.get("/api/pipeline/list/{status}")
async def pipelines(status: str, auth: bool = Depends(authenticate)):
    q = (
        db.Pipeline.join(db.User)
        .join(db.PipelineDefinition)
        .join(db.PipelineImage)
        .select()
    )
    q = q.where(db.Pipeline.c.status == status.upper())
    q = q.order_by(db.Pipeline.c.id.desc())
    res = await database.fetch_all(q)
    return res


@app.post("/api/pipeline/update/{uid}")
async def pipeline_update(uid: int, status: Status, auth: bool = Depends(authenticate)):
    new = status.status.upper()
    heartbeat = status.heartbeat or {}

    q = db.Pipeline.select().where(db.Pipeline.c.id == uid)
    res = await database.fetch_one(q)
    if res.status == "TO_CANCEL" and new == "RUNNING":
        return {"id": uid, "status": res.status}

    q = (
        db.Pipeline.update()
        .where(db.Pipeline.c.id == uid)
        .values(status=new, heartbeat=heartbeat)
    )
    await database.execute(q)

    return {"id": uid, "status": new}


@app.get("/api/storage/get/{name}")
async def get_storage(name: str, auth: bool = Depends(authenticate)):
    name = name.replace("__", "/")
    parts = Path(name).parts
    for i in range(len(parts)):
        p = Path(*parts[: i + 1])
        q = db.Storage.select().where(db.Storage.c.mountPath == f"{p}")
        res = await database.fetch_one(q)
        if res:
            output = {"spec": res["spec"]}
            relpath = Path(name).relative_to(res["mountPath"])
            output["spec"]["volumeMount"]["mountPath"] = str(
                Path(output["spec"]["volumeMount"]["mountPath"]) / relpath
            )
            if "nfs" in output["spec"]["volume"]:
                output["spec"]["volume"]["nfs"]["path"] = str(
                    Path(output["spec"]["volume"]["nfs"]["path"]) / relpath
                )
            return output
    return {"detail": "Not found"}


@app.get("/api/pipeline/logs/{uid}")
async def get_logs(
    uid: int, offset: int, limit: int, auth: bool = Depends(authenticate)
):
    res = await es.search(
        index="logstash-*",
        query={
            "bool": {
                "must": {
                    "match": {
                        "kubernetes.pod_name": {
                            "query": f"daskjob-{uid}-runner",
                            "operator": "AND",
                        }
                    }
                },
                # "filter": [{"match": {"message": "INFO"}}],
            }
        },
        size=limit,
        from_=offset,
        sort={
            "@timestamp": {
                "order": "desc",
                "format": "strict_date_hour_minute_second_fraction",
            }
        },
    )
    return res
