import logging.config
import logging.handlers

logconf = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "standard": {
            "class": "jsonformatter.JsonFormatter",
            # "format": "%(asctime)s [%(levelname)s] %(name)s: %(message)s",
            "format": '{"name": "name","levelno": "levelno","levelname": "levelname","pathname": "pathname","filename": "filename","module": "module","lineno": "lineno","funcName": "funcName","created": "created","asctime": "asctime","msecs": "msecs","relativeCreated": "relativeCreated","thread": "thread","threadName": "threadName","process": "process","message": "message"}',
            "datefmt": "%Y-%m-%d %H:%M:%S",
        },
        "simple": {"format": "%(levelname)s %(message)s"},
    },
    "handlers": {
        "default": {
            "level": "DEBUG",
            "formatter": "standard",
            "class": "logging.StreamHandler",
        },
        # "file": {
        #     "level": "DEBUG",
        #     "formatter": "standard",
        #     "class": "logging.handlers.RotatingFileHandler",
        #     "filename": "/var/log/owl/scheduler.log",
        #     "maxBytes": 1024 * 1024 * 10,
        #     "backupCount": 5,
        # },
    },
    "loggers": {
        "owl2": {"handlers": ["default"], "level": "DEBUG", "propagate": True},
        "owl2.scheduler": {
            "handlers": ["default"],
            "level": "DEBUG",
            "propagate": False,
        },
        "owl2.pipeline": {
            "handlers": ["default"],
            "level": "DEBUG",
            "propagate": False,
        },
        "distributed.client": {
            "handlers": ["default"],
            "level": "INFO",
            "propagate": False,
        },
        "distributed.scheduler": {
            "handlers": ["default"],
            "level": "INFO",
            "propagate": False,
        },
        "distributed.worker": {
            "handlers": ["default"],
            "level": "INFO",
            "propagate": False,
        },
        "distributed.utils_perf": {
            "handlers": ["default"],
            "level": "INFO",
            "propagate": False,
        },
        "uvicorn": {"handlers": ["default"], "level": "DEBUG", "propagate": False},
        "py.warnings": {
            "handlers": ["default"],
            "level": "WARNING",
            "propagate": False,
        },
        "uvicorn.access": {
            "handlers": ["default"],
            "level": "DEBUG",
            "propagate": False,
        },
        "uvicorn.error": {
            "handlers": ["default"],
            "level": "DEBUG",
            "propagate": False,
        },
    },
}


def initlog():
    """Setup logging configuration.

    Parameters
    ----------
    cfg : dict
        Dictionary containing logging configuration.
    """
    logging.captureWarnings(True)
    logging.config.dictConfig(logconf)
