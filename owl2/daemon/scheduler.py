import asyncio
import json
import logging
import os
import time
from contextlib import suppress
from pathlib import Path
from typing import Any, Dict

import aiohttp
import zmq
import zmq.asyncio
from aiohttp.client_exceptions import ClientConnectorError
from async_timeout import timeout

from .. import k8s
from ..config import config
from ..config import refresh as refresh_config
from ..schema import schema_pipeline
from ..utils import safe_loop


class Scheduler:
    """Owl pipeline scheduler.

    The scheduler is responsible of communication between the user
    and the pipeline. It queues pipeline requests and runs them as
    resoures become available.
    """

    def __init__(self):
        self.logger = logging.getLogger("owl2.scheduler")

        self.heartbeat = config["heartbeat"]
        self.started = False
        self._token = config["secret_token"]
        self._max_pipe = config["maximum_concurrent_pipelines"]
        self._tasks = []  # list of coroutines
        self.pipelines = {}  # list of pipelines running
        self.loop = asyncio.get_event_loop()
        self.loop.run_until_complete(self.start())

    @property
    def namespace(self) -> str:
        """Return namespace of running Pod

        Returns
        -------
        Name of namespace where the current pod is running.
        """
        if not hasattr(self, "_namespace"):
            try:
                self._namespace = open(
                    "/var/run/secrets/kubernetes.io/serviceaccount/namespace"
                ).read()
            except FileNotFoundError:
                self._namespace = k8s.get_current_namespace()
        return self._namespace

    async def start(self):
        """Start scheduler.

        Start the following:

        - Check that we have correct Kubernetes credentials
        - Setup connection sockets.
        - HTTP client session for connections to the API.
        - Task for checking pipelines in the queue.
        - Task for checking pipelines status.
        - Task for receiving log messages.
        """
        if self.started:
            await asyncio.sleep(0)
            return

        self.logger.debug("Starting scheduler")

        await self.check_k8s_credentials()

        try:
            await self._setup_sockets()
            await self._start_liveness_probe()
        except Exception as e:
            self.logger.critical("Unable to setup communication sockets: %s", e)
            raise

        self.session = aiohttp.ClientSession()
        await self.register_token()

        self._start_tasks()

        self.started = True
        self.logger.info("Owl scheduler succesfully started")

    async def check_k8s_credentials(self):
        """Check that we have correct Kubernetes credentials."""
        try:
            self.logger.debug("Testing Kubernetes credentials")
            await k8s.kube_test_credentials()
        except k8s.ConfigException as e:
            self.logger.critical("Unable to obtain Kubernetes configuration: %s", e)
            raise
        except k8s.ApiException as e:
            self.logger.critical("Exception when calling API: %s", e)
            raise
        except Exception:
            raise

    def _start_tasks(self):
        """Add tasks to the loop."""
        self._tasks.extend(
            [
                asyncio.create_task(self.load_pipelines()),
                asyncio.create_task(self.scheduler_heartbeat()),
                asyncio.create_task(self.status_pipelines()),
                asyncio.create_task(self.clean_pipelines()),
                asyncio.create_task(self.cancel_pipelines()),
                # asyncio.create_task(self.query_prometheus()),
                # asyncio.create_task(self.logging_protocol()),
                # asyncio.create_task(self.admin_commands()),
                # asyncio.create_task(self.live_commands()),
            ]
        )

    @safe_loop()
    async def register_token(self):
        """Register token with the API."""
        path = "/api/auth/token/handshake"
        res = await self._api_request(path, method="GET")
        with suppress(KeyError, TypeError):
            if res["detail"] == "ok":
                self.logger.info("Token registered with the API")
                return True
        self.logger.info("Waiting for API to be ready")
        await asyncio.sleep(self.heartbeat)

    async def _setup_sockets(self):
        """Setup ZMQ sockets.

        We create the sockets for bidirectional communication with
        the pipelines (ROUTER-DEALER) and for receiving the logs
        (PUB-SUB).

        These are TCP sockets listening in the ports configured by
        environmental variables `OWL_SERVICE_PORT_PIPE` and
        `OWL_SERVICE_PORT_ADMIN`.
        """
        self.ctx = zmq.asyncio.Context()

        # pipelines -- used for bidirectional messages with the pipelines
        self.pipe_addr = f"tcp://0.0.0.0:{config['scheduler_service_port_pipe']}"
        self.pipe_router = self.ctx.socket(zmq.ROUTER)
        self.pipe_router.set(zmq.ROUTER_HANDOVER, 1)
        self.logger.debug("Pipeline router address: %s", self.pipe_addr)
        self.pipe_router.bind(self.pipe_addr)

        # administrative -- to get messages from the admin user through the API
        self.admin_addr = f"tcp://0.0.0.0:{config['scheduler_service_port_admin']}"
        self.admin_router = self.ctx.socket(zmq.ROUTER)
        self.admin_router.set(zmq.ROUTER_HANDOVER, 1)
        self.logger.debug("Admin router address: %s", self.admin_addr)
        self.admin_router.bind(self.admin_addr)

        await asyncio.sleep(0)

    async def _start_liveness_probe(self):
        # liveness and readiness probes
        self.live_addr = f"tcp://0.0.0.0:{config['scheduler_service_port_live']}"
        self.live_router = self.ctx.socket(zmq.STREAM)
        self.live_router.bind(self.live_addr)
        self.logger.debug("Liveness probe address: %s", self.live_addr)
        await asyncio.sleep(0)

    async def _api_request(
        self, path: str, method: str = None, data: Dict[str, Any] = None
    ):
        method = method or "GET"
        url = f"http://{config['api_service_host']}:{config['api_service_port']}{path}"
        headers = {"Authorization": f"Bearer {self._token}"}
        try:
            async with timeout(self.heartbeat):
                if method == "GET":
                    async with self.session.get(url, headers=headers) as resp:
                        response = await resp.json()
                elif method == "POST":
                    data = data or {}
                    async with self.session.post(
                        url, json=data, headers=headers
                    ) as resp:
                        response = await resp.json()
        except ClientConnectorError:
            self.logger.error("Unable to connect to API at %s", url)
            return
        except asyncio.TimeoutError:
            self.logger.error("API request took too long. Cancelled")
            return
        return response

    async def stop(self):
        """Stop scheduler.

        One by one, cancel all tasks and stop the scheduler.

        Notes:
        ------
        - Pipelines are not cancelled, they are allowed to finish.

        """

        self.logger.debug("Stopping scheduler")
        if self.started:
            for task in self._tasks:
                task.cancel()
                with suppress(asyncio.CancelledError):
                    await task

            await self.session.close()
            self.started = False

        with suppress(Exception):
            self.pipe_router.close(linger=0)
            self.admin_router.close(linger=0)
            self.live_router.close(linger=0)

        self.logger.info("Owl scheduler succesfully stopped")

    @safe_loop()
    async def scheduler_heartbeat(self):
        """Scheduler Heartbeat.

        Logs number of tasks and pipelines priodically.
        """
        await asyncio.sleep(self.heartbeat)
        self._tasks = [task for task in self._tasks if not task.done()]
        self.logger.debug(
            "Tasks %s, Pipelines %s", len(self._tasks), len(self.pipelines)
        )

    @safe_loop()
    async def load_pipelines(self):
        """Pipeline loader. Query pipelines from API and starts new."""

        await asyncio.sleep(self.heartbeat)

        # We can set maintenance mode at runtime
        if Path("/var/run/owl/nopipe").exists():
            self.logger.debug("Maintenaince mode. Pipelines not started.")
            return

        # Maximum number of pipelines at runtime
        if (maxpipe := Path("/var/run/owl/maxpipe")).exists():
            with maxpipe.open() as fh:
                self._max_pipe = int(fh.read()) or self._max_pipe

        self.logger.debug("Checking for running pipelines")
        pipelines = await self._api_request("/api/pipeline/list/running")

        for pipe in pipelines:
            if pipe["id"] in self.pipelines:
                continue
            await self.add_pipeline(pipe)

        self.logger.debug("Checking for pending pipelines")
        pipelines = await self._api_request("/api/pipeline/list/pending")

        if not pipelines:
            return

        if "detail" in pipelines:
            self.logger.error(pipelines["detail"])
            return

        # We sort the pipelines by priority
        with suppress(Exception):
            pipelines = sorted(pipelines, key=lambda x: x["priority"], reverse=True)

        # We start the pipelines that are not running
        for pipe in pipelines:
            if pipe["id"] in self.pipelines:
                self.logger.debug("Pipeline already in list %s", pipe["id"])
                continue
            if len(self.pipelines) >= self._max_pipe:
                self.logger.debug("Maximum number of pipelines reached")
                break

            try:
                pipe["config"] = schema_pipeline(pipe["config"])
            except Exception:
                self.logger.error("Unable to parse pipeline config %s", pipe["config"])
                await self.update_pipeline(pipe["id"], "ERROR")
                continue

            await self.start_pipeline(pipe)

    async def add_pipeline(self, pipe: Dict[str, Any]):
        uid = int(pipe["id"])
        pipe_config = pipe["config"]
        jobname = f"pipeline-{uid}"
        status = "RUNNING"
        heartbeat = {"status": status}

        self.pipelines[uid] = {
            "last": time.monotonic(),
            "heartbeat": heartbeat,
            "job": status,
            "name": jobname,
            "uid": uid,
            "pdef": pipe_config,
            "config": pipe_config,
            "pipeline_name": pipe.get("pipeline_name", ""),
            "pipeline_pkg": pipe.get("package_name", ""),
        }

        self._tasks.append(asyncio.create_task(self.heartbeat_pipeline(uid)))

        await asyncio.sleep(0)

    async def start_pipeline(self, pipe: Dict[str, Any]):
        """Start a pipeline.

        Parameters
        ----------
        pipe: dict
            Pipeline data from the API.

        """
        jobid = int(pipe["id"])
        username = pipe["username"]
        pipeline_name = pipe["pipeline_name"]
        self.logger.debug("Starting pipeline %s : %s", jobid, pipe)

        await self._tear_pipeline(jobid)

        self.pipelines[jobid] = {
            "config": pipe["config"],
            "pipeline_name": pipeline_name,
            "pipeline_pkg": pipe["package_name"],
        }

        os.environ.update({"USER": username})
        refresh_config()

        resources = pipe["config"]["resources"]
        self.logger.debug("Configuring resources %s", resources)
        storage = []
        for vol in resources["volumes"]:
            vol_safe = vol.replace("/", "__")
            res = await self._api_request(f"/api/storage/get/{vol_safe}")
            if "spec" in res:
                this = res["spec"]
                storage.append(res["spec"])

        limits = requests = {"cpu": resources["cpu"], "memory": resources["memory"]}

        await k8s.dask_job_create(
            jobid,
            "python:3.10",
            environment=[
                {"name": "JOBID", "value": f"{jobid}"},
                {"name": "OWL_USER", "value": username},
                {
                    "name": "PYTHON_VIRTUALENV",
                    "value": "owl2_" + pipeline_name.replace("-", "_"),
                },
                # {"name": "RUN_DEVELOP", "value": "devel"},
                {"name": "NB_USER", "value": username},
            ],
            resources={
                "limits": limits,
                "requests": requests,
            },
            replicas=resources["workers"],
            namespace=self.namespace,
            storage=storage,
        )

        now = time.monotonic()
        self.pipelines[jobid]["start"] = now
        self.pipelines[jobid]["last"] = now
        self._tasks.append(asyncio.create_task(self.heartbeat_pipeline(jobid)))

    @safe_loop()
    async def admin_commands(self):
        """Receive and execute administrative commands."""
        token, msg = await self.admin_router.recv_multipart()
        token, msg = token.decode(), json.loads(msg.decode())
        self.logger.debug("Received administrative command %s", msg)
        # In maintenance mode no new pipelines are scheduled
        if "maintenance" in msg:
            self._set_maintance(msg["maintenance"])
        # Limit maximum number of pipelines that can be run at the same time
        elif "maxpipe" in msg:
            self._set_maxpipe(msg["maxpipe"])
        # Configure heartbeat
        elif "heartbeat" in msg:
            self._set_heartbeat(msg["heartbeat"])
        # Cancel pipeline
        elif "stop_pipeline" in msg:
            try:
                uid = int(msg["jobid"])
                status = "CANCELLED"
            except Exception:
                return
            if uid in self.pipelines:
                await self.stop_pipeline(uid, status)
            else:
                await self.update_pipeline(uid, status)

    @safe_loop()
    async def heartbeat_pipeline(self, jobid):
        """Pipeline Heartbeat.

        Check if the pipeline is still running and update the API.
        """
        await asyncio.sleep(self.heartbeat)

        if jobid not in self.pipelines:
            return True

        # We check the kubernetes job status
        try:
            body = await k8s.dask_job_status(jobid, self.namespace)
        except k8s.ApiException:
            self.logger.error("Unable to get job status")
            return

        self.pipelines[jobid]["status"] = body
        msg = {
            "config": self.pipelines[jobid]["config"],
            "pipeline_pkg": self.pipelines[jobid]["pipeline_pkg"],
        }

        self.logger.debug("Sending heartbeat to pipeline %s", jobid)
        await self.pipe_router.send_multipart(
            [
                str(jobid).encode("utf-8"),
                json.dumps(msg).encode("utf-8"),
            ]
        )

    async def stop_pipeline(self, uid: int, status: str):
        """Stop pipeline

        Parameters
        ----------
        uid : int
            Unique ID of pipeline
        status : str
            status of the pipeline
        """
        self.logger.debug(f"Stopping pipeline ID {uid}")
        if uid in self.pipelines:
            await self._tear_pipeline(uid)
            await self.update_pipeline(uid, status)
            del self.pipelines[uid]

    async def _tear_pipeline(self, jobid: int):
        while True:
            try:
                await k8s.dask_job_delete(jobid, self.namespace)
                await asyncio.sleep(self.heartbeat // 2)
            except k8s.ApiException:
                break
        await asyncio.sleep(self.heartbeat // 2)
        self.logger.debug("Deleted job %s", jobid)

    @safe_loop()
    async def status_pipelines(self):
        uid, msg = await self.pipe_router.recv_multipart()
        uid, msg = int(uid.decode()), json.loads(msg.decode())
        self.logger.debug("Received heartbeat from pipeline %s : %s", uid, msg)
        self.pipelines[uid].update({"last": time.monotonic(), "heartbeat": msg})
        status = msg["status"]
        if status in ["ERROR", "FINISHED"]:
            await self.stop_pipeline(uid, status)
        else:
            await self.update_pipeline(uid, status)

    @safe_loop()
    async def clean_pipelines(self):
        # Remove pipelines that do not respond at the heartbeat rate
        await asyncio.sleep(self.heartbeat)
        for jobid in list(self.pipelines):  # list -> avoid dictionary changet
            watch = time.monotonic() - self.pipelines[jobid]["last"]
            if watch > self.heartbeat * 5:
                self.logger.error("Pipeline %s did not respond in time", jobid)
                await self.stop_pipeline(jobid, "ERROR")

    @safe_loop()
    async def cancel_pipelines(self):
        """Query for pipelines to cancel."""
        await asyncio.sleep(self.heartbeat)
        self.logger.debug("Checking for pipelines to cancel")

        pipelines = await self._api_request("/api/pipeline/list/to_cancel")

        if not pipelines:
            return

        if "detail" in pipelines:
            self.logger.error(pipelines["detail"])
            return

        for pipe in list(pipelines):
            if pipe["id"] in self.pipelines:
                await self.stop_pipeline(pipe["id"], "CANCELLED")
            else:
                await self.update_pipeline(pipe["id"], "CANCELLED")

    async def update_pipeline(self, uid: int, status: str):
        """Update pipeline status.

        Parameters
        ----------
        uid
            pipeline id
        status
            status to set the pipeline to
        response
            heartbeat response
        """
        await asyncio.sleep(0)

        self.logger.info("Updating pipeline %s - %s", uid, status)
        data = {"status": status}
        with suppress(KeyError):
            data["heartbeat"] = self.pipelines[uid]["heartbeat"]
            data["heartbeat"]["jobstatus"] = self.pipelines[uid]["status"]

        await self._api_request(f"/api/pipeline/update/{uid}", method="POST", data=data)
