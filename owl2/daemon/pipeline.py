import asyncio
import json
import logging
import os
import sys
import time
from asyncio.subprocess import PIPE, STDOUT
from concurrent.futures import ThreadPoolExecutor

import zmq
import zmq.asyncio

from ..config import config
from ..utils import safe_loop


class Pipeline:
    """Pipeline worker.

    The pipeline worker is started by the scheduler when a pipeline needs to
    be run. This worker runs the pipeline code, starting the swarm containers if
    necessart and is responsible of following the execution status and cleaning up.

    The process of running a pipeline is then as follows:

    1. The main scheduler starts a pipeline worker and sends the pipeline definition
       file and extra configuration needed.
    2. The worker loads the pipeline code and validates it against its schema.
    3. If running in swarm mode, the cluster starts the docker containers as requested.
    4. The worker starts a separate thread and runs the pipeline code.
    5. The main thread listens for heartbeat connections from the scheduler and waits for
       pipeline completion.
    6. The worker responds with the pipeline completion result to the scheduler.
    7. The scheduler stops the pipeline worker.

    Parameters
    ----------
    conf
        configuration
    logconf
        logging configuration
    """

    def __init__(self):
        self.logger = logging.getLogger("owl2.pipeline")

        self.executor = ThreadPoolExecutor(max_workers=3)
        # self.executor = ProcessPoolExecutor(max_workers=3)
        self.started = False
        self._tasks = []
        self.config = None
        self.package_name = None
        self.uid = os.environ["JOBID"]
        self.status = "RUNNING"
        self.result = None
        self.info = {"started": time.time(), "version": ""}
        self._watch = time.monotonic()
        self.loop = asyncio.get_event_loop()
        self.loop.run_until_complete(self.start())

    @property
    def elapsed(self):
        return time.monotonic() - self._watch

    async def start(self):
        await self._setup_sockets()

        self._tasks.append(asyncio.ensure_future(self.heartbeat()))
        self.logger.debug("Waiting for pipeline to start...")

        await self._install_pipeline()

        from owl2 import pipelines

        if self.name not in dir(pipelines):
            self.logger.error("Pipeline %s not found", self.name)
            self.status = "ERROR"
            return

        func = getattr(pipelines, self.name)
        self.info["version"] = func.__version__
        self.logger.info("Starting pipeline %s", self.name)
        self.proc = self.loop.run_in_executor(
            self.executor, func, self.config, self.loop
        )
        self.proc.add_done_callback(self.pipeline_done)
        self._tasks.append(self.proc)

        self.started = True
        self.status = "RUNNING"
        self.logger.info("Pipeline succesfully started")

    async def _setup_sockets(self):
        self.owl_host = config["scheduler_service_host"]
        self.pipe_port = config["scheduler_service_port_pipe"]
        self.pipe_addr = f"tcp://{self.owl_host}:{self.pipe_port}"
        self.logger.debug("Connecting to %s", self.pipe_addr)
        self.ctx = zmq.asyncio.Context()

        self.pipe_socket = self.ctx.socket(zmq.DEALER)
        self.pipe_socket.setsockopt(zmq.IDENTITY, self.uid.encode("utf-8"))
        self.pipe_socket.connect(self.pipe_addr)

        await asyncio.sleep(1)

    @safe_loop()
    async def heartbeat(self):
        msg = await self.pipe_socket.recv()
        if self.config is None:
            msg = json.loads(msg.decode())
            self.config = msg["config"]
            self.config["pipeline_pkg"] = msg["pipeline_pkg"]
            self.package_name = msg["pipeline_pkg"]
            self.name = self.config["name"]

        response = {
            "status": self.status,
            "result": self.result,
            "started": self.info["started"],
            "elapsed": self.elapsed,
            "version": self.info["version"],
        }

        await self.pipe_socket.send(json.dumps(response).encode("utf-8"))

    async def _install_pipeline(self):
        while self.package_name is None:
            await asyncio.sleep(10)

        self.logger.info("Installing pipeline %s", self.package_name)
        cmd = [sys.executable, "-m", "pip", "install", "-U", self.package_name]
        process = await asyncio.create_subprocess_shell(
            " ".join(cmd), stdin=PIPE, stdout=PIPE, stderr=STDOUT
        )
        stdout, stderr = await process.communicate()
        self.logger.info("Pipeline %s installed", self.package_name)
        self.logger.debug("stdout: %s", stdout)
        self.logger.debug("stderr: %s", stderr)

    def pipeline_done(self, future: asyncio.Future):
        if e := future.exception():
            self.status = "ERROR"
            self.result = f"{e}"
        else:
            self.status = "FINISHED"
            self.result = future.result()
        self.logger.info(
            "Pipeline ID %s finished - %s - %s ", self.uid, self.status, self.result
        )
