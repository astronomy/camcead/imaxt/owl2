import functools


def safe_loop():
    """Run coroutine in a safe loop.

    The coroutine runs in a 'while True' loop
    and exceptions logged.
    """

    def wrapper(func):
        @functools.wraps(func)
        async def wrapped(*args):
            logger = args[0].logger
            while True:
                try:
                    res = await func(*args)
                    if res is True:
                        break
                except Exception:
                    logger.error("Unkown exception", exc_info=True)

        return wrapped

    return wrapper


class AttrDict(dict):
    """Attribute dictionary.

    Allows to access dictionary items by attribute.

    Parameters
    ----------
    d : Dict[Any, Any]
        input dictionary

    Examples
    --------
    >>> d = {'a': 1, 'b': 2}
    >>> ad = AttrDict(d)
    >>> ad.a
    ... 1
    """

    def __getattr__(self, item):
        return self.get(item, None)

    def __getstate__(self):
        return self

    def __setstate__(self, state):
        self.update(state)
