import sqlalchemy

metadata = sqlalchemy.MetaData()

Pipeline = sqlalchemy.Table(
    "owl_pipeline",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("config", sqlalchemy.JSON, nullable=False),
    sqlalchemy.Column("heartbeat", sqlalchemy.JSON, nullable=True),
    sqlalchemy.Column(
        "owner_id",
        sqlalchemy.Integer,
        sqlalchemy.ForeignKey("auth_user.id"),
        nullable=False,
    ),
    sqlalchemy.Column(
        "pdef_id",
        sqlalchemy.Integer,
        sqlalchemy.ForeignKey("owl_pipelinedefinition.id"),
        nullable=False,
    ),
    sqlalchemy.Column(
        "status", sqlalchemy.String(length=16), default="PENDING", nullable=False
    ),
    sqlalchemy.Column("created_on", sqlalchemy.DateTime, nullable=False),
    sqlalchemy.Column("started_at", sqlalchemy.DateTime, nullable=True),
    sqlalchemy.Column("finished_at", sqlalchemy.DateTime, nullable=True),
    sqlalchemy.Column("elapsed", sqlalchemy.Float, nullable=True),
)

PipelineDefinition = sqlalchemy.Table(
    "owl_pipelinedefinition",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("env", sqlalchemy.JSON, nullable=False),
    sqlalchemy.Column(
        "name", sqlalchemy.String(length=32), nullable=False, unique=True
    ),
    sqlalchemy.Column("pipeline_name", sqlalchemy.String(length=32)),
    sqlalchemy.Column("package_name", sqlalchemy.String(length=32)),
    sqlalchemy.Column(
        "docker_image_id",
        sqlalchemy.Integer,
        sqlalchemy.ForeignKey("owl_pipelineimage.id"),
        nullable=False,
    ),
    # sqlalchemy.Column("active", sqlalchemy.Boolean, default=True),
    sqlalchemy.Column("version", sqlalchemy.String(length=32), nullable=False),
)

PipelineImage = sqlalchemy.Table(
    "owl_pipelineimage",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("image", sqlalchemy.String(length=80), nullable=False),
    sqlalchemy.Column("tag", sqlalchemy.String(length=80), nullable=False),
    sqlalchemy.Column("image_spec", sqlalchemy.JSON, nullable=False),
)

Storage = sqlalchemy.Table(
    "s3storage_storage",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("name", sqlalchemy.String(length=80), nullable=False),
    sqlalchemy.Column("spec", sqlalchemy.JSON, nullable=False),
    sqlalchemy.Column("mountPath", sqlalchemy.String(length=80), nullable=False),
)

User = sqlalchemy.Table(
    "auth_user",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("username", sqlalchemy.String(length=32), unique=True),
    sqlalchemy.Column("is_active", sqlalchemy.Boolean, default=True),
)

UserAdmin = sqlalchemy.Table(
    "auth_user",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("username", sqlalchemy.String(length=32), unique=True),
    sqlalchemy.Column("password", sqlalchemy.String(length=255), unique=True),
    sqlalchemy.Column("is_superuser", sqlalchemy.Boolean, default=False),
    sqlalchemy.Column("is_staff", sqlalchemy.Boolean, default=False),
    sqlalchemy.Column("is_active", sqlalchemy.Boolean, default=True),
    extend_existing=True,
)
