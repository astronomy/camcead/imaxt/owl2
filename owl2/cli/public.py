from argparse import Namespace

import uvicorn
from uvicorn.config import LOGGING_CONFIG

from owl2.config import config


def run_public_api(args: Namespace) -> None:  # pragma: nocover
    """Start the API service.

    Parameters
    ----------
    arg
        Argparse namespace containing command line flags.
    """
    from owl2.log import logconf
    from owl2.public import app

    LOGGING_CONFIG.update(logconf)
    api_port = int(config["public_api_service_port"])

    uvicorn.run(app, host="0.0.0.0", port=api_port)
