import asyncio
import logging
import logging.config
import signal
from argparse import Namespace

from owl2.daemon import Pipeline
from owl2.log import initlog

logger = logging.getLogger("owl2")


def run_pipeline(args: Namespace) -> None:
    """Run Pipeline.

    Parameters
    ----------
    arg
        Argparse namespace containing command line flags.
    """
    initlog()

    try:
        start_loop()
    except Exception as e:  # noqa
        logger.critical("Error running the pipeline : %s", e, exc_info=True)
        raise

    logger.info("Pipeline run ended.")


def start_loop():
    loop = asyncio.get_event_loop()
    logger.info("Starting Pipeline")
    pipeline = Pipeline()

    loop.add_signal_handler(signal.SIGTERM, loop.stop)
    loop.add_signal_handler(signal.SIGINT, loop.stop)
    loop.add_signal_handler(signal.SIGHUP, lambda *args: None)

    loop.run_forever()
    loop.run_until_complete(pipeline.stop())

    loop.close()
