import io
import os
import sys
from argparse import ArgumentParser, FileType, Namespace
from typing import List

from .api import run_api
from .pipeline import run_pipeline
from .public import run_public_api
from .scheduler import run_scheduler


def parse_args(input: List[str]) -> Namespace:
    """Parse command line arguments.

    Parameters
    ----------
    input
        list of command line arguments

    Returns
    -------
    parsed arguments
    """
    parser = ArgumentParser()
    subparsers = parser.add_subparsers()

    # Scheduler
    scheduler = subparsers.add_parser("scheduler")
    scheduler.add_argument("--conf", required=False, type=FileType("r"))
    scheduler.set_defaults(func=run_scheduler)

    # Run the API
    api = subparsers.add_parser("api")
    api.add_argument("--conf", required=False, type=FileType("r"))
    api.set_defaults(func=run_api)

    # Run the API
    api_public = subparsers.add_parser("public-api")
    api_public.add_argument("--conf", required=False, type=FileType("r"))
    api_public.set_defaults(func=run_public_api)

    # Run a pipeline
    run = subparsers.add_parser("pipeline")
    run.add_argument("--conf", required=False, type=FileType("r"))
    run.set_defaults(func=run_pipeline)

    args = parser.parse_args(input)
    if not hasattr(args, "func"):
        parser.print_help()

    return args


def main():
    """Main entry point for owl.

    Invoke the command line help with::

        $ owl-server --help

    """
    # initlog("cli")

    args = parse_args(sys.argv[1:])

    if hasattr(args, "func"):
        args.func(args)
