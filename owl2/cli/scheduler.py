import asyncio
import logging
import logging.config
import signal
from argparse import Namespace
from pathlib import Path

# from owl2.config import config  # noqa: F401
from owl2.daemon import Scheduler
from owl2.log import initlog

logger = logging.getLogger("owl2")


def run_scheduler(args: Namespace) -> None:
    """Run scheduler.

    Parameters
    ----------
    arg
        Argparse namespace containing command line flags.
    """
    initlog()

    try:
        start_loop()
    except Exception:
        logger.critical("Error starting the scheduler", exc_info=True)


def start_loop():
    loop = asyncio.get_event_loop()
    logger.info("Starting Owl Scheduler")
    scheduler = Scheduler()

    loop.add_signal_handler(signal.SIGTERM, loop.stop)
    loop.add_signal_handler(signal.SIGINT, loop.stop)
    loop.add_signal_handler(signal.SIGHUP, lambda *args: None)

    loop.run_forever()
    loop.run_until_complete(scheduler.stop())

    loop.close()
