from argparse import Namespace

import uvicorn
from uvicorn.config import LOGGING_CONFIG

from owl2.config import config


def run_api(args: Namespace) -> None:  # pragma: nocover
    """Start the API service.

    Parameters
    ----------
    arg
        Argparse namespace containing command line flags.
    """
    from owl2.api import app
    from owl2.log import logconf

    LOGGING_CONFIG.update(logconf)
    api_port = int(config["api_service_port"])

    uvicorn.run(app, host="0.0.0.0", port=api_port)
