import logging
import random
import string

from kubernetes_asyncio import client
from kubernetes_asyncio import config as config
from kubernetes_asyncio.client.api_client import ApiClient
from kubernetes_asyncio.client.rest import ApiException  # noqa: F401
from kubernetes_asyncio.config.config_exception import ConfigException  # noqa: F401

import owl2.config


async def kube_test_credentials():
    """Check that we have correct Kubernetes credentials.

    If you get an error on this call don't proceed. Something is wrong on your Kubernetes
    configuration.
    """
    try:
        config.load_incluster_config()
    except ConfigException:
        await config.load_kube_config()

    async with ApiClient() as api:
        v1 = client.BatchV1Api(api)
        await v1.get_api_resources()


def make_scheduler_spec(jobname, image, environment=None, storage=None):
    storage = storage or []
    environment = environment or []
    volumes = owl2.config.get("defaultVolumes", []) + owl2.config.get(
        "extraVolumes", []
    )

    volume_mounts = owl2.config.get("defaultVolumeMounts", []) + owl2.config.get(
        "extraVolumeMounts", []
    )
    for vol in storage:
        volumes.append(vol["volume"])
        volume_mounts.append(vol["volumeMount"])

    return {
        "spec": {
            "containers": [
                {
                    "name": "scheduler",
                    "image": image,
                    "imagePullPolicy": "IfNotPresent",
                    "command": ["/init/init.sh"],
                    "args": ["dask-scheduler"],
                    "ports": [
                        {"name": "tcp-comm", "containerPort": 8786, "protocol": "TCP"},
                        {
                            "name": "http-dashboard",
                            "containerPort": 8787,
                            "protocol": "TCP",
                        },
                    ],
                    # "resources": {
                    #     "limits": {"cpu": "2", "memory": "6Gi"},
                    #     "requests": {"cpu": "2", "memory": "6Gi"},
                    # },
                    "readinessProbe": {
                        "httpGet": {"port": "http-dashboard", "path": "/health"},
                        "initialDelaySeconds": 5,
                        "periodSeconds": 10,
                    },
                    "livenessProbe": {
                        "httpGet": {"port": "http-dashboard", "path": "/health"},
                        "initialDelaySeconds": 15,
                        "periodSeconds": 20,
                    },
                    "env": owl2.config.get("default_env", []) + environment,
                    "volumeMounts": volume_mounts,
                }
            ],
            "volumes": volumes,
        },
        "service": {
            "type": "ClusterIP",
            "selector": {
                "dask.org/component": "scheduler",
                "dask.org/cluster-name": jobname,
            },
            "ports": [
                {
                    "name": "tcp-comm",
                    "port": 8786,
                    "targetPort": "tcp-comm",
                    "protocol": "TCP",
                },
                {
                    "name": "http-dashboard",
                    "port": 8787,
                    "targetPort": "http-dashboard",
                    "protocol": "TCP",
                },
            ],
        },
    }


def make_worker_spec(image, environment=None, replicas=1, storage=None, resources=None):
    storage = storage or []
    environment = environment or []
    volumes = owl2.config.get("defaultVolumes", []) + owl2.config.get(
        "extraVolumes", []
    )

    volume_mounts = owl2.config.get("defaultVolumeMounts", []) + owl2.config.get(
        "extraVolumeMounts", []
    )
    for vol in storage:
        volumes.append(vol["volume"])
        volume_mounts.append(vol["volumeMount"])

    return {
        "replicas": replicas,
        "spec": {
            "containers": [
                {
                    "name": "worker",
                    "image": image,
                    "imagePullPolicy": "IfNotPresent",
                    "command": ["/init/init.sh"],
                    "args": [
                        "dask-worker",
                        "--name",
                        "$(DASK_WORKER_NAME)",
                        "--nworkers",
                        "1",
                        "--nthreads",
                        resources.get("limits", {}).get("cpu", "1"),
                        "--memory-limit",
                        resources.get("limits", {}).get("memory", "1Gi"),
                        "--dashboard",
                        "--dashboard-address",
                        "8788",
                        "--death-timeout",
                        "60",
                    ],
                    "ports": [
                        {
                            "name": "http-dashboard",
                            "containerPort": 8788,
                            "protocol": "TCP",
                        }
                    ],
                    "resources": resources or {},
                    "env": owl2.config.get("default_env", []) + environment,
                    "volumeMounts": volume_mounts,
                }
            ],
            "volumes": volumes,
        },
    }


def make_job_spec(image, environment=None, storage=None):
    storage = storage or []
    environment = environment or []
    volumes = owl2.config.get("defaultVolumes", []) + owl2.config.get(
        "extraVolumes", []
    )

    volume_mounts = owl2.config.get("defaultVolumeMounts", []) + owl2.config.get(
        "extraVolumeMounts", []
    )
    for vol in storage:
        volumes.append(vol["volume"])
        volume_mounts.append(vol["volumeMount"])

    return {
        "containers": [
            {
                "name": "job",
                "image": image,
                "imagePullPolicy": "IfNotPresent",
                "command": ["/init/init.sh"],
                "args": ["owl-server", "pipeline"],
                "env": owl2.config.get("default_env", []) + environment,
                "volumeMounts": volume_mounts,
                # "resources": {
                #     "limits": {"cpu": "2", "memory": "6Gi"},
                #     "requests": {"cpu": "2", "memory": "6Gi"},
                # },
            }
        ],
        "restartPolicy": "Never",
        "volumes": volumes,
    }


def make_daskjob_spec(
    jobid, image, environment=None, resources=None, replicas=1, storage=None
):
    jobname = f"daskjob-{jobid}"
    return {
        "apiVersion": "kubernetes.dask.org/v1",
        "kind": "DaskJob",
        "metadata": {"name": jobname, "namespace": "owl"},
        "spec": {
            "job": {
                "spec": make_job_spec(image, environment=environment, storage=storage),
            },
            "cluster": {
                "spec": {
                    "worker": make_worker_spec(
                        image,
                        environment=environment,
                        replicas=replicas,
                        storage=storage,
                        resources=resources,
                    ),
                    "scheduler": make_scheduler_spec(
                        jobname, image, environment=environment, storage=storage
                    ),
                }
            },
        },
    }


async def dask_job_create(
    jobid,
    image,
    environment=None,
    resources=None,
    replicas=1,
    namespace="default",
    storage=None,
):
    """Create a DaskJob on Kubernetes.

    Parameters
    ----------
    jobid : str
        Unique identifier for the job.
    image : str
        Docker image to use for the job.
    environment : dict
        Environment variables to pass to the job.
    resources : dict
        Resource requirements for the job.
    replicas : int
        Number of replicas to create.
    namespace : str
        Kubernetes namespace to create the job in.
    storage : list
        List of storage mounts in the job.

    """
    spec = make_daskjob_spec(
        jobid,
        image,
        environment=environment,
        resources=resources,
        replicas=replicas,
        storage=storage,
    )
    async with ApiClient() as api:
        v1 = client.CustomObjectsApi(api)
        body = await v1.create_namespaced_custom_object(
            "kubernetes.dask.org",
            "v1",
            namespace,
            "daskjobs",
            spec,
        )
    return body


async def dask_job_delete(jobid, namespace):
    """Delete a DaskJob on Kubernetes.

    Parameters
    ----------
    jobid : str
        Unique identifier for the job.
    """
    jobname = f"daskjob-{jobid}"
    async with ApiClient() as api:
        v1 = client.CustomObjectsApi(api)
        await v1.delete_namespaced_custom_object(
            "kubernetes.dask.org",
            "v1",
            namespace,
            "daskjobs",
            jobname,
        )


async def dask_job_get(jobid, namespace):
    """Get a DaskJob on Kubernetes.

    Parameters
    ----------
    jobid : str
        Unique identifier for the job.
    """
    jobname = f"daskjob-{jobid}"
    async with ApiClient() as api:
        v1 = client.CustomObjectsApi(api)
        body = await v1.get_namespaced_custom_object(
            "kubernetes.dask.org",
            "v1",
            namespace,
            "daskjobs",
            jobname,
        )
    return body


async def dask_job_status(jobid, namespace):
    """Get status for a DaskJob on Kubernetes.

    Parameters
    ----------
    jobid : str
        Unique identifier for the job.
    """
    body = await dask_job_get(jobid, namespace)
    return body["status"]


async def dask_job_list(namespace):
    """List DaskJobs on Kubernetes."""
    async with ApiClient() as api:
        v1 = client.CustomObjectsApi(api)
        body = await v1.list_namespaced_custom_object(
            "kubernetes.dask.org",
            "v1",
            namespace,
            "daskjobs",
        )
    return body


async def dask_job_logs(jobid, namespace):
    """Get logs for a DaskJob on Kubernetes.

    Parameters
    ----------
    jobid : str
        Unique identifier for the job.
    """
    jobname = f"daskjob-{jobid}"
    async with ApiClient() as api:
        v1 = client.CoreV1Api(api)
        body = await v1.read_namespaced_pod_log(
            jobname,
            namespace,
        )
    return body


async def dask_pod_status(jobid, namespace):
    """Get status for a DaskJob on Kubernetes.

    Parameters
    ----------
    jobid : str
        Unique identifier for the job.
    """
    jobname = f"daskjob-{jobid}"
    async with ApiClient() as api:
        v1 = client.CoreV1Api(api)
        body = await v1.read_namespaced_pod_status(
            jobname,
            namespace,
        )
    return body


async def dask_job_scale(jobid, replicas):
    """Scale a DaskJob on Kubernetes.

    Parameters
    ----------
    jobid : str
        Unique identifier for the job.
    replicas : int
        Number of replicas to create.
    """
    jobname = f"daskjob-{jobid}"
    async with ApiClient() as api:
        v1 = client.CustomObjectsApi(api)
        body = await v1.patch_namespaced_custom_object_scale(
            "kubernetes.dask.org",
            "v1",
            "owl",
            "daskjobs",
            jobname,
            {
                "spec": {
                    "replicas": replicas,
                }
            },
        )
    return body


async def dask_job_scale_up(jobid):
    """Scale a DaskJob on Kubernetes.

    Parameters
    ----------
    jobid : str
        Unique identifier for the job.
    """
    jobname = f"daskjob-{jobid}"
    async with ApiClient() as api:
        v1 = client.CustomObjectsApi(api)
        body = await v1.patch_namespaced_custom_object_scale(
            "kubernetes.dask.org",
            "v1",
            "owl",
            "daskjobs",
            jobname,
            {
                "spec": {
                    "replicas": 1,
                }
            },
        )
    return body


async def dask_job_scale_down(jobid):
    """Scale a DaskJob on Kubernetes.

    Parameters
    ----------
    jobid : str
        Unique identifier for the job.
    """
    jobname = f"daskjob-{jobid}"
    async with ApiClient() as api:
        v1 = client.CustomObjectsApi(api)
        body = await v1.patch_namespaced_custom_object_scale(
            "kubernetes.dask.org",
            "v1",
            "owl",
            "daskjobs",
            jobname,
            {
                "spec": {
                    "replicas": 0,
                }
            },
        )
    return body


async def dask_job_scale_to(jobid, replicas):
    """Scale a DaskJob on Kubernetes.

    Parameters
    ----------
    jobid : str
        Unique identifier for the job.
    replicas : int
        Number of replicas to create.
    """
    jobname = f"daskjob-{jobid}"
    async with ApiClient() as api:
        v1 = client.CustomObjectsApi(api)
        body = await v1.patch_namespaced_custom_object_scale(
            "kubernetes.dask.org",
            "v1",
            "owl",
            "daskjobs",
            jobname,
            {
                "spec": {
                    "replicas": replicas,
                }
            },
        )
    return body


async def dask_job_scale_up_to(jobid, replicas):
    """Scale a DaskJob on Kubernetes.

    Parameters
    ----------
    jobid : str
        Unique identifier for the job.
    replicas : int
        Number of replicas to create.
    """
    jobname = f"daskjob-{jobid}"
    async with ApiClient() as api:
        v1 = client.CustomObjectsApi(api)
        body = await v1.patch_namespaced_custom_object_scale(
            "kubernetes.dask.org",
            "v1",
            "owl",
            "daskjobs",
            jobname,
            {
                "spec": {
                    "replicas": replicas,
                }
            },
        )
    return body


async def dask_job_scale_down_to(jobid, replicas):
    """Scale a DaskJob on Kubernetes.

    Parameters
    ----------
    jobid : str
        Unique identifier for the job.
    replicas : int
        Number of replicas to create.
    """
    jobname = f"daskjob-{jobid}"
    async with ApiClient() as api:
        v1 = client.CustomObjectsApi(api)
        body = await v1.patch_namespaced_custom_object_scale(
            "kubernetes.dask.org",
            "v1",
            "owl",
            "daskjobs",
            jobname,
            {
                "spec": {
                    "replicas": replicas,
                }
            },
        )
    return body


def get_current_namespace():
    """Get the current namespace from the Kubernetes configuration.

    Returns
    -------
    namespace : str
        The current namespace.
    """
    return config.list_kube_config_contexts()[1]["context"]["namespace"]
