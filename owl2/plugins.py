import asyncio
import json

from distributed import SchedulerPlugin, WorkerPlugin
from loky import ProcessPoolExecutor


class WorkerLoggingPlugin(WorkerPlugin):
    def __init__(self, config):
        self.config = config

    def setup(self, worker):
        import logging.config

        with open("/tmp/logconfig", "w") as f:
            json.dump(self.config, f)

        logging.config.dictConfig(self.config)


class SchedulerLoggingPlugin(SchedulerPlugin):
    def __init__(self, config):
        self.config = config

    async def start(self, scheduler):
        import logging.config

        logging.config.dictConfig(self.config)
        await asyncio.sleep(0)


class ProcessPoolPlugin(WorkerPlugin):
    def setup(self, worker):
        executor = ProcessPoolExecutor(max_workers=worker.nthreads)
        worker.executors["processes"] = executor


class PipInstall(WorkerPlugin):
    def __init__(self, packages):
        self.packages = packages

    def require_install(self, package: str):
        import pkg_resources
        from pkg_resources import DistributionNotFound, VersionConflict

        if package.startswith("git+") or package.startswith("http"):
            return True

        try:
            pkg_resources.require(package)
        except (Exception, DistributionNotFound, VersionConflict):
            return True

        return False

    async def setup(self, worker):
        import asyncio
        import sys

        for package in self.packages:
            if self.require_install(package):
                cmd = [sys.executable, "-m", "pip", "install", "-U", package]
                process = await asyncio.create_subprocess_shell(" ".join(cmd))
                await process.communicate()

            await asyncio.sleep(0)
